using UnityEngine;
using UnityEngine.InputSystem;

namespace Blackthorne.Inputs
{
    [DisallowMultipleComponent]
    public sealed class PlayerInput : MonoBehaviour
    {
        private PlayerInputActions actions;

        private void Awake() => InitializeActions();

        private void Start() => BindAllActions();

        private void OnEnable() => actions.Enable();

        private void OnDisable() => actions.Disable();

        private void InitializeActions()
        {
            actions = new PlayerInputActions();
        }

        private void BindAllActions()
        {
            BindMoveAction();
            BindRunAction();
            BindJumpAction();
            BindEquipWeaponAction();
        }

        private void BindMoveAction()
        {
            var moveable = GetComponent<IMoveable>();
            if (moveable == null) return;

            void Move(InputAction.CallbackContext action)
            {
                var input = action.ReadValue<Vector2>();
                // Stick Deadzone Processor is not working.
                input = ApplyDeadzone(input, min: 0.5F);
                moveable.Move(input);
            }

            var action = actions.Player.Move;
            action.started += Move;
            action.performed += Move;
            action.canceled += Move;
        }

        private void BindJumpAction()
        {
            var jumpable = GetComponent<IJumpable>();
            if (jumpable == null) return;

            var action = actions.Player.Jump;
            action.started += _ => jumpable.Jump();
        }

        private void BindRunAction()
        {
            var runable = GetComponent<IRunnable>();
            if (runable == null) return;

            var action = actions.Player.Run;
            action.started += _ => runable.StartRun();
            action.canceled += _ => runable.CancelRun();
        }

        private void BindEquipWeaponAction()
        {
            var equippable = GetComponent<IEquippable>();
            if (equippable == null) return;

            var action = actions.Player.EquipWeapon;
            action.started += _ => equippable.Toggle();
        }

        public static Vector2 ApplyDeadzone(Vector2 input, float min)
        {
            var minHorzValue = Mathf.Abs(input.x) < min;
            var minVertValue = Mathf.Abs(input.y) < min;

            input.x = minHorzValue ? 0F : input.x;
            input.y = minVertValue ? 0F : input.y;

            return input;
        }
    }
}