namespace Blackthorne
{
    /// <summary>
    /// Interfaces used on objects able to run.
    /// </summary>
    public interface IRunnable
    {
        /// <summary>
        /// Starts to run.
        /// </summary>
        void StartRun();

        /// <summary>
        /// Cancels the last run.
        /// </summary>
        void CancelRun();
    }
}