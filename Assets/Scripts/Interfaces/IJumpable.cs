namespace Blackthorne
{
    /// <summary>
    /// Interface used on objects able to jump.
    /// </summary>
    public interface IJumpable
    {
        void Jump();
    }
}