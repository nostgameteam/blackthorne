using UnityEngine;

namespace Blackthorne
{
    /// <summary>
    /// Interface used on objects able to move.
    /// </summary>
    public interface IMoveable
    {
        /// <summary>
        /// Moves using the given direction.
        /// </summary>
        /// <param name="direction"></param>
        void Move(Vector2 direction);
    }
}