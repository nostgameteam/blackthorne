namespace Blackthorne
{
    /// <summary>
    /// Interface used on objects able to be equipped.
    /// </summary>
    public interface IEquippable
    {
        /// <summary>
        /// Equips this object.
        /// </summary>
        void Equip();

        /// <summary>
        /// Unequips this object.
        /// </summary>
        void Unequip();

        /// <summary>
        /// Toggles this object equipment.
        /// </summary>
        void Toggle();

        /// <summary>
        /// Checks if this object is equipped.
        /// </summary>
        /// <returns></returns>
        bool IsEquipped();
    }
}