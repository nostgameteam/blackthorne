using UnityEngine;
using ActionCode.AABB;

namespace Blackthorne.Physics
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(BoundingBox2D))]
    public sealed class RootBody : MonoBehaviour
    {
        [SerializeField, Tooltip("The local Animator component.")]
        private Animator animator;
        [SerializeField, Tooltip("Axis Aligned Bounding Box used to detect collisions.")]
        private BoundingBox2D boundingBox;
        [SerializeField, Tooltip("The layer mask for collisions.")]
        private LayerMask collision = -1; // Everything

        #region Physical Properties
        /// <summary>
        /// Axis Aligned Bounding Box used to detect collisions.
        /// </summary>
        public BoundingBox2D BoundingBox => boundingBox;

        /// <summary>
        /// Current horizontal direction.
        /// </summary>
        public float HorizontalDirection => BoundingBox.ForwardDirection.x;

        /// <summary>
        /// The total size of the box.
        /// </summary>
        public Vector3 Size => boundingBox.Size;

        /// <summary>
        /// The half size of the box.
        /// </summary>
        public Vector3 HalfSize => Size * 0.5F;

        /// <summary>
        /// The offset of the box.
        /// </summary>
        public Vector3 Offset => boundingBox.Offset;

        /// <summary>
        /// The root velocity.
        /// </summary>
        public Vector3 Velocity => animator.velocity;

        public Vector2 AbsoluteVelocityPerSecond { get; private set; }

        public float HorizontalSpeed => Velocity.x;

        public float VerticalSpeed => Velocity.y;

        public bool IsFalling { get; private set; }

        public bool RestrictVerticalCollisions { get; set; }

        public bool RestrictHorizontalCollisions { get; set; }
        #endregion

        #region Collision Hits Properties
        /// <summary>
        /// Top collision hit info.
        /// </summary>
        public RaycastHitInfo TopHit { get; private set; }

        /// <summary>
        /// Right collision hit info.
        /// </summary>
        public RaycastHitInfo RightHit { get; private set; }

        /// <summary>
        /// Left collision hit info.
        /// </summary>
        public RaycastHitInfo LeftHit { get; private set; }

        /// <summary>
        /// Bottom collision hit info.
        /// </summary>
        public RaycastHitInfo BottomHit { get; private set; }
        #endregion

        public const int VERTICAL_RAYS_COUNT = 3;
        public const int HORIZONTAL_RAYS_COUNT = 3;
        public const float COLLISION_SKIN = 0.0001F;
        private readonly Color COLLISION_COLOR = Color.red;
        private readonly Color UNRESTRICT_COLLISION_COLOR = Color.blue;

        private Vector3 currentPosition;

        private void Reset()
        {
            animator = GetComponent<Animator>();
            boundingBox = GetComponent<BoundingBox2D>();

            animator.applyRootMotion = true;
            animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
        }

        private void Awake() => RestrictCollisions();

        private void OnAnimatorMove()
        {
            currentPosition = animator.rootPosition;

            AbsoluteVelocityPerSecond = new Vector2(
                Mathf.Abs(HorizontalSpeed),
                Mathf.Abs(VerticalSpeed)) * Time.deltaTime;

            UpdateHorizontalCollisions();
            RestrictHorizontalMovement();

            UpdateVerticalCollisions();
            RestrictVerticalMovement();

            UpdateExternalMovement();

            transform.position = currentPosition;
            transform.rotation = animator.rootRotation;
        }

        private void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying)
            {
                UpdateVerticalCollisions();
                UpdateHorizontalCollisions();
            }
            DrawCollisions();
        }

        #region Collision Methods
        /// <summary>
        /// Checks if the Box is solid colliding on top. Triggers don't count as a solid collision.
        /// </summary>
        /// <returns>True if the Box is solid colliding on top.</returns>
        public bool IsTopCollision() => TopHit.IsSolidColliding();

        /// <summary>
        /// Checks if the Box is solid colliding on right. Triggers don't count as a solid collision.
        /// </summary>
        /// <returns>True if the Box is solid colliding on right.</returns>
        public bool IsRightCollision() => RightHit.IsSolidColliding();

        /// <summary>
        /// Checks if the Box is solid colliding on left. Triggers don't count as a solid collision.
        /// </summary>
        /// <returns>True if the Box is solid colliding on left.</returns>
        public bool IsLeftCollision() => LeftHit.IsSolidColliding();

        /// <summary>
        /// Checks if the Box is solid colliding on bottom. Triggers don't count as a solid collision.
        /// </summary>
        /// <returns>True if the Box is solid colliding on bottom.</returns>
        public bool IsBottomCollision() => BottomHit.IsSolidColliding();

        /// <summary>
        /// Checks a tile collision. Triggers don't count as collision.
        /// <para>
        /// Your collider should be used by a Composite Collider with a GeometryType 
        /// type set to <see cref="CompositeCollider2D.GeometryType.Polygons"/>
        /// </para>
        /// </summary>
        /// <param name="position"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        public bool IsTileCollision(Vector2 position, bool draw = false)
            => boundingBox.IsOverlapingPoint(position, collision, draw);

        /// <summary>
        /// Checks if the Box is grounded (i.e., Box is colliding on bottom).
        /// </summary>
        /// <returns>True if grounded. False otherwise.</returns>
        public bool IsGrounded() => IsBottomCollision();

        /// <summary>
        /// Checks if able to move forward.
        /// </summary>
        /// <returns>Whether able to move forward.</returns>
        public bool IsAbleToMoveForward() => IsAbleToHorizontalMove(HorizontalDirection);

        /// <summary>
        /// Checks if able to move horizontally based on the given direction.
        /// </summary>
        /// <param name="direction">Horizontal direction.</param>
        /// <returns>Whether able to move horizontally.</returns>
        public bool IsAbleToHorizontalMove(float direction)
        {
            return
                direction > 0f && IsAbleToMoveRight() ||
                direction < 0f && IsAbleToMoveLeft();
        }

        /// <summary>
        /// Checks if able to move right.
        /// </summary>
        /// <returns>Whether able to right move.</returns>
        public bool IsAbleToMoveRight() => !IsRightCollision();

        /// <summary>
        /// Checks if able to move left.
        /// </summary>
        /// <returns>Whether able to left move.</returns>
        public bool IsAbleToMoveLeft() => !IsLeftCollision();

        /// <summary>
        /// Fixes the body exactly on ground.
        /// </summary>
        public void FixIntoGround()
        {
            var hit = boundingBox.VerticalCast(Vector3.down, 100F, collision);
            if (hit.IsSolidColliding())
            {
                var pos = transform.position;
                pos.y = hit.point.y;
                transform.position = pos;
            }
        }

        private void UpdateHorizontalCollisions()
        {
            const float slopeLimit = 0F;

            var distance = HalfSize.x + AbsoluteVelocityPerSecond.x + COLLISION_SKIN;
            var topOffset = Vector3.zero;
            var bottomOffset = Vector3.up * 0.001f;

            LeftHit = BoundingBox.HorizontalCast(Vector3.left, topOffset, bottomOffset, distance, collision, slopeLimit, HORIZONTAL_RAYS_COUNT);
            RightHit = BoundingBox.HorizontalCast(Vector3.right, topOffset, bottomOffset, distance, collision, slopeLimit, HORIZONTAL_RAYS_COUNT);
        }

        private void UpdateVerticalCollisions()
        {
            var distance = HalfSize.y + AbsoluteVelocityPerSecond.y + COLLISION_SKIN;
            var leftOffset = Vector3.left * (AbsoluteVelocityPerSecond.x * HorizontalDirection - 0.01F);
            var rightOffset = Vector3.left * (AbsoluteVelocityPerSecond.x * HorizontalDirection + 0.01F);

            TopHit = BoundingBox.VerticalCast(Vector3.up, leftOffset, rightOffset, distance, collision, VERTICAL_RAYS_COUNT);
            BottomHit = BoundingBox.VerticalCast(Vector2.down, leftOffset, rightOffset, distance, collision, VERTICAL_RAYS_COUNT);
        }

        private void RestrictHorizontalMovement()
        {
            if (!RestrictHorizontalCollisions) return;

            if (IsRightCollision() && IsGoingRight())
            {
                var horztHitPos = RightHit.point.x - HalfSize.x - Offset.x;
                currentPosition.x = RoundBy025(horztHitPos);
            }
            else if (IsLeftCollision() && IsGoingLeft())
            {
                var horztHitPos = LeftHit.point.x + HalfSize.x + Offset.x;
                currentPosition.x = RoundBy025(horztHitPos);
            }
        }

        private void RestrictVerticalMovement()
        {
            if (!RestrictVerticalCollisions) return;

            if (IsTopCollision() && IsGoingUp())
            {
                var vertHitPos = TopHit.point.y - HalfSize.y - Offset.y;
                currentPosition.y = RoundBy025(vertHitPos);
            }
            else if (IsBottomCollision() && IsGoingDown())
            {
                var vertHitPos = BottomHit.point.y + HalfSize.y - Offset.y;
                currentPosition.y = RoundBy025(vertHitPos);
            }
        }

        private void UpdateExternalMovement()
        {
            //TODO implement
            /*
            var surface = BottomHit.GetComponent<SurfaceEffector2D>();
            if (surface != null)
            {
                var velocity = surface.speed * ??.direction;
                VerticalSpeed = velocity.y;
                HorizontalSpeed = velocity.x;
            }*/
        }

        private void DrawCollisions()
        {

            if (IsTopCollision()) BoundingBox.DrawTopLine(COLLISION_COLOR);
            if (IsLeftCollision()) BoundingBox.DrawLeftLine(COLLISION_COLOR);
            if (IsRightCollision()) BoundingBox.DrawRightLine(COLLISION_COLOR);
            if (IsBottomCollision()) BoundingBox.DrawBottomLine(COLLISION_COLOR);

            if (Application.isPlaying)
            {
                if (!RestrictHorizontalCollisions)
                {
                    BoundingBox.DrawLeftLine(UNRESTRICT_COLLISION_COLOR);
                    BoundingBox.DrawRightLine(UNRESTRICT_COLLISION_COLOR);
                }
                if (!RestrictVerticalCollisions)
                {
                    BoundingBox.DrawTopLine(UNRESTRICT_COLLISION_COLOR);
                    BoundingBox.DrawBottomLine(UNRESTRICT_COLLISION_COLOR);
                }
            }
        }
        #endregion

        public bool IsGoingRight() => HorizontalSpeed > 0f;

        public bool IsGoingLeft() => HorizontalSpeed < 0f;

        public bool IsGoingUp() => VerticalSpeed > 0f;

        public bool IsGoingDown() => VerticalSpeed < 0f || IsFalling;

        public void ToggleRestrictCollisions()
        {
            if (IsCollisionsRestricted()) UnrestrictCollisions();
            else RestrictCollisions();
        }

        public void RestrictCollisions()
        {
            RestrictVerticalCollisions = true;
            RestrictHorizontalCollisions = true;
        }

        public void UnrestrictCollisions()
        {
            RestrictVerticalCollisions = false;
            RestrictHorizontalCollisions = false;
        }

        public bool IsCollisionsRestricted()
            => RestrictVerticalCollisions || RestrictHorizontalCollisions;

        /// <summary>
        /// Rounds the given number by 0.25
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static float RoundBy025(float number)
        {
            return Mathf.Round(number * 4F) / 4F;
        }
    }
}