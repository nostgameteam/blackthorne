﻿#if UNITY_EDITOR
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace Blackthorne.Editor
{
    public class ZipBuild
    {
        [PostProcessBuild(1)]
        public static async void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
        {
            if (target != BuildTarget.WebGL) return;

            var zipPath = Path.HasExtension(pathToBuiltProject) ?
                pathToBuiltProject + ".zip" :
                Path.ChangeExtension(pathToBuiltProject, "zip");

            Debug.LogFormat("Zipping folder {0} to {1}", pathToBuiltProject, zipPath);
            await Task.Run(() => ZipFile.CreateFromDirectory(pathToBuiltProject, zipPath));
            Debug.LogFormat("Zip process has finished.");
        }
    }
}
#endif
