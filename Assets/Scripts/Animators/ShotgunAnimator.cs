using UnityEngine;

namespace Blackthorne.Animators
{
    [DisallowMultipleComponent]
    public sealed class ShotgunAnimator : AbstractAnimator, IEquippable
    {
        #region Animator Parameters
        private const string SHOTGUN_LAYER_NAME = "Shotgun";
        private readonly int EQUIP_SHOTGUN = Animator.StringToHash("equipShotgun");
        private readonly int UNEQUIP_SHOTGUN = Animator.StringToHash("unequipShotgun");
        private readonly int SHOTGUN_EQUIPPED = Animator.StringToHash("shotgunEquipped");
        private readonly int SHOTGUN_MULTIPLIER = Animator.StringToHash("shotgunMultiplier");
        #endregion

        private int shotgunLayerIndex;

        private void Awake()
            => FindShotgunLayerIndex();

        private void Start() => CheckForSpawned();

        public void Toggle()
        {
            if (IsEquipped()) Unequip();
            else Equip();
        }

        public void Equip() => animator.SetTrigger(EQUIP_SHOTGUN);

        public void Unequip() => animator.SetTrigger(UNEQUIP_SHOTGUN);

        public bool IsEquipped() => GetShotgunLayerWeight() > 0.5f;

        // Called by Kyle equip shotgun animations
        public void ToggleShotgunLayer()
        {
            if (IsEquipped()) DisableShotgunLayer();
            else EnableShotgunLayer();
        }

        private void FindShotgunLayerIndex()
            => shotgunLayerIndex = animator.GetLayerIndex(SHOTGUN_LAYER_NAME);

        private void SetShotgunLayer(float weight)
            => animator.SetLayerWeight(shotgunLayerIndex, weight);

        private float GetShotgunLayerWeight()
            => animator.GetLayerWeight(shotgunLayerIndex);

        private void EnableShotgunLayer()
        {
            SetShotgunLayer(1F);
            SetShotgunEquipped(true);
            SetShotgunMultiplier(2f);
        }

        private void DisableShotgunLayer()
        {
            SetShotgunLayer(0F);
            SetShotgunEquipped(false);
            SetShotgunMultiplier(1f);
        }

        private void SetShotgunEquipped(bool isEquipped)
            => animator.SetBool(SHOTGUN_EQUIPPED, isEquipped);

        private void SetShotgunMultiplier(float multiplier)
            => animator.SetFloat(SHOTGUN_MULTIPLIER, multiplier);

        private void CheckForSpawned()
        {
            bool isSpawnstate = animator.GetCurrentAnimatorStateInfo(0).IsName("Normal.Spawn");
            if (isSpawnstate) EnableShotgunLayer();
        }
    }
}