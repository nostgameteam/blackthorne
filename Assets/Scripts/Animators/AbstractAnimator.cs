using UnityEngine;

namespace Blackthorne.Animators
{
    /// <summary>
    /// Abstract animator component.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public abstract class AbstractAnimator : MonoBehaviour
    {
        [SerializeField, Tooltip("The local Animator component.")]
        protected Animator animator;

        protected virtual void Reset()
        {
            animator = GetComponent<Animator>();
        }
    }
}