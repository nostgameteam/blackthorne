using UnityEngine;
using Blackthorne.Physics;

namespace Blackthorne.Animators
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RootBody))]
    [RequireComponent(typeof(ShotgunAnimator))]
    public sealed class ClimbAnimator : AbstractAnimator, IJumpable
    {
        [SerializeField] private RootBody body;
        [SerializeField] private ShotgunAnimator shotgunAnimator;

        [Header("Climb Offsets")]
        public Vector2 climbUpOffset = new Vector2(0.25f, 4f);
        public Vector2 climbDownOffset = new Vector2(0.25f, -1f);

        #region Animator Parameters
        private readonly int JUMP = Animator.StringToHash("jump");
        private readonly int CAN_CLIMB_UP = Animator.StringToHash("canClimbUp");
        private readonly int CAN_CLIMB_DOWN = Animator.StringToHash("canClimbDown");
        #endregion

        private const string IDLE_STATE_NAME = "Normal.Stand.Idle.Idle-Normal";
        private const string IDLE_EXTENDED_STATE_NAME = "Normal.Stand.Idle.Idle-Extended.Idle-Extended-Normal";
        private const string RUN_STATE_NAME = "Normal.Locomotion.Ground-Locomotion.Run.Run-Loop";

        protected override void Reset()
        {
            base.Reset();
            body = GetComponent<RootBody>();
            shotgunAnimator = GetComponent<ShotgunAnimator>();
        }

        private void Update()
        {
            var canClimbUp = CanClimbUp();
            var canClimbDown = CanClimbDown();

            SetCanClimbUp(canClimbUp);
            SetCanClimbDown(canClimbDown);
        }

        private void OnDrawGizmosSelected()
        {
            CanClimbUp(drawCollisions: true);
            CanClimbDown(drawCollisions: true);
        }

        public void Jump()
        {
            if (CanJump()) animator.SetTrigger(JUMP);
        }

        private bool CanClimbUp(bool drawCollisions = false)
        {
            var climbUpPosition = transform.position + transform.TransformDirection(climbUpOffset);
            var hasClimbUpTile = body.IsTileCollision(climbUpPosition, drawCollisions);
            var hasTileOver = body.IsTileCollision(climbUpPosition + Vector3.up, drawCollisions);
            var hasTileLeft = body.IsTileCollision(climbUpPosition - transform.right, drawCollisions);
            var canClimbUp = hasClimbUpTile && !hasTileOver && !hasTileLeft;
            return canClimbUp;
        }

        private bool CanClimbDown(bool drawCollisions = false)
        {
            var climbDownPosition = transform.position + transform.TransformDirection(climbDownOffset);
            var hasClimbDownTile = body.IsTileCollision(climbDownPosition, drawCollisions);
            var hasTileBellow = body.IsTileCollision(climbDownPosition + Vector3.down, drawCollisions);
            var hasTileLeft = body.IsTileCollision(climbDownPosition - transform.right, drawCollisions);
            var canClimbDown = hasClimbDownTile && !hasTileBellow && !hasTileLeft;
            return canClimbDown;
        }

        private void SetCanClimbUp(bool canClimbUp) => animator.SetBool(CAN_CLIMB_UP, canClimbUp);

        private void SetCanClimbDown(bool canClimbDown) => animator.SetBool(CAN_CLIMB_DOWN, canClimbDown);

        private bool CanJump() => CanJumpWhenIdle() || CanJumpWhenRunning();

        private bool CanJumpWhenIdle()
        {
            var isOnIdleState = 
                IsSatateName(IDLE_STATE_NAME) || 
                IsSatateName(IDLE_EXTENDED_STATE_NAME);
            return isOnIdleState && !shotgunAnimator.IsEquipped();
        }

        private bool CanJumpWhenRunning() => IsSatateName(RUN_STATE_NAME);

        private bool IsSatateName(string name)
            => animator.GetCurrentAnimatorStateInfo(0).IsName(name);
    }
}