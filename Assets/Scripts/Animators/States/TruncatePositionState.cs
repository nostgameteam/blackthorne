using System;
using UnityEngine;

namespace Blackthorne.Animators
{
    /// <summary>
    /// Truncates the GameObject position by 0.25
    /// </summary>
    public class TruncatePositionState : StateMachineBehaviour
    {
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateExit(animator, stateInfo, layerIndex);

            var position = animator.transform.position;
            position.x = RoundBy025(position.x);
            UpdateAnimatorPosition(animator, position);
        }

        /// <summary>
        /// Updates the given animator by the given position.
        /// </summary>
        /// <param name="animator"></param>
        /// <param name="position"></param>
        private static void UpdateAnimatorPosition(Animator animator, Vector3 position)
        {
            animator.transform.position = position;
            animator.Update(0f);
        }

        /// <summary>
        /// Rounds the given number by 0.25
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static float RoundBy025(float number)
        {
            return Mathf.Round(number * 4F) / 4F;
        }
    }
}