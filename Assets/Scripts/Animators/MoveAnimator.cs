using UnityEngine;
using Blackthorne.Physics;

namespace Blackthorne.Animators
{
    /// <summary>
    /// Animator controller component used to move.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RootBody))]
    public sealed class MoveAnimator : AbstractAnimator, IMoveable, IRunnable
    {
        [SerializeField, Tooltip("The local RootBody component.")]
        private RootBody body;

        #region Animator Parameters
        private readonly int RUN_INPUTTING = Animator.StringToHash("runInputting");
        private readonly int UP_INPUTTING = Animator.StringToHash("upInputting");
        private readonly int HORIZ_INPUTTING = Animator.StringToHash("horizInputting");
        private readonly int DOWN_INPUTTING = Animator.StringToHash("downInputting");
        private readonly int TURN_INPUTTING = Animator.StringToHash("turnInputting");

        private readonly int CAN_MOVE_FORWARD = Animator.StringToHash("canMoveForward");
        private readonly int GROUNDED = Animator.StringToHash("grounded");
        private readonly int USING_LEFT_LEG = Animator.StringToHash("usingLeftLeg");
        #endregion

        private Vector2 lastInput;

        protected override void Reset()
        {
            base.Reset();
            body = GetComponent<RootBody>();
        }

        private void Update()
        {
            UpdateMovementParams();
            UpdatePhysicsParameters();
        }

        public void Move(Vector2 input) => lastInput = input;

        public void StartRun() => SetRunInputting(true);

        public void CancelRun() => SetRunInputting(false);

        // Called by Kyle walk left animations
        public void EnableUsingLeftLeg() => SetUsingLeftLeg(true);

        // Called by Kyle walk right animations
        public void DisableUsingLeftLeg() => SetUsingLeftLeg(false);

        private void SetRunInputting(bool isRunning)
            => animator.SetBool(RUN_INPUTTING, isRunning);

        private void SetUpInputting(bool isUpInput)
            => animator.SetBool(UP_INPUTTING, isUpInput);

        private void SetHorizontalInputting(bool isHorizInput)
            => animator.SetBool(HORIZ_INPUTTING, isHorizInput);

        private void SetDownInputting(bool isDownInput)
            => animator.SetBool(DOWN_INPUTTING, isDownInput);

        private void SetTurnInputting(bool isTurnInput)
            => animator.SetBool(TURN_INPUTTING, isTurnInput);

        private void SetCanMoveForward(bool canMoveForward)
            => animator.SetBool(CAN_MOVE_FORWARD, canMoveForward);

        private void SetGrounded(bool isGrounded)
            => animator.SetBool(GROUNDED, isGrounded);

        private void SetUsingLeftLeg(bool usingLeftLeg)
            => animator.SetBool(USING_LEFT_LEG, usingLeftLeg);

        private void UpdateMovementParams()
        {
            var upInput = lastInput.y > 0f;
            var downInput = lastInput.y < 0f;
            var horizInput = Mathf.Abs(lastInput.x) > 0f;
            var turnInput = lastInput.x * body.HorizontalDirection < 0f;

            SetUpInputting(upInput);
            SetDownInputting(downInput);
            SetTurnInputting(turnInput);
            SetHorizontalInputting(horizInput);
        }

        private void UpdatePhysicsParameters()
        {
            SetGrounded(body.IsGrounded());
            SetCanMoveForward(body.IsAbleToMoveForward());
        }
    }
}